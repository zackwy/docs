---
id: feature-flags
title: Feature flags
---

Feature flags allow for features to be turned on and off with the flick of a switch. This allows for more precise control over releases and deprecations. Feature flags can also target specific user IDs, browsers, and other categories such that features can be rolled out to specific subsets of a user-base only.

We use GrowthBook for feature flags, which are tied to Experiments heavily, so when interacting with them via code, you will be using experiments services / managers to check the state.

## Setting up

### Declare a new feature

To declare a new feature, you need to add it to GrowthBook via the Features tab. At this stage you specify a Feature Key which serves as an identifier for access within the app. You should also specify a description so that others know what your feature flag controls, and set the value type (usually on/off) and the default value of the flag.

### Override Rules

Override rules allow you to extend behavior of a feature flag to

* Force a specify value globally.
* Rollout the feature to a percentage of users.
* Tie the feature to an experiment.

#### $CONFIG

To configure the Minds front and back end, you will need to add the features endpoint to your settings.php. This will be read from to determine what features are running, and cached to save load on the endpoint.

```php
$CONFIG->set('growthbook', [
    'features_endpoint' => 'https://growthbook-api.minds.com/api/features/key_c98323bd51a6b3ab',
]);
```

## Usage

### Backend

```php
$experimentsManager = Di::_()->get('Experiments\Manager');
$experimentsManager->setUser($loggedInUser);

if ($experimentsManager->isOn('my-cool-feature')) {
  // Cool backend feature
}
```

### Frontend 

#### Component Controller
```js
  if (this.experiments.hasVariation('my-cool-feature', true)) {
    // code for when experiment is active
  }
```
#### HTML Templates

```html
<div *mExperiment="'my-cool-feature'; variation: true"
  <my-cool-feature></my-cool-feature>
</div>
```

### Mobile

Mobile is using the Growthbook React libary directly - information on usage can be [found here](https://docs.growthbook.io/lib/react#usefeature-hook).
