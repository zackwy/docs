---
id: superhero
title: Superhero
---

> We are currently building out this section

## What is a Superhero

A Superhero is a critical site outage that requires immediate attention. We use [PagerDuty](https://pagerduty.com) to handle our on-call schedules and notification routing. 

## Core features

Superhero events should only be triggered when **core** features have major degraded functionality that is impacting the **majority** of the user base. 

### Examples of a core feature outage

- Minds.com is returning 50X errors and is inaccessible
- Users are consistently unable to register or login to the site and this can be reliably reproduced
- Users newsfeeds are inaccessible and will not load
- Users are unable to create posts

### Examples of of non core features

The following examples can be created with the "Priority::Urgent" label and resolved during office hours, but should not be consider Superhero events:

- Rewards were not issued
- Push notifications are not being delivered or are delayed
- Rich embed thumbnails are not displaying on posts
- Analytics are showing no results, or they are inaccurate

### Minds Chat

Minds Chat relies on [Synapse](https://github.com/matrix-org/synapse) which provides limited scaling abilities and **no high availability** support. Until these fundamental technical issues are resolved, it's stability is consider outside of Superhero support. 


## How to declare a Superhero

Follow the diagram below to determine if a Superhero should be called:

[![Superhero diagram](assets/superhero.png "Diagram of Minds Superhero")
Click to enlarge](assets/superhero.png)

1. Always open a new browser window, clear all cookies and session data.
2. Ensure you are not in Canary mode or have any canary cookies set
3. Leave a message in the Superhero room
4. Create an new issue at [gitlab.com/minds/minds](gitlab.com/minds/minds) with the **Superhero** template.
5. The template will automatically apply the **Type::Superhero** and **Superhero::Triggered** labels.
6. Pagerduty will automatically be triggered via Gitlab. The Gitlab issue should be treated as the central communication hub with the **#superhero** room on Zulip used for additional offline support.

## Runbooks

> We are currently in the process of creating runbooks for on-call issues. We will update this section when they are ready.

